# Miscellaneous Recipes

This Minecraft data pack adds recipes for crafting or cutting various
items that may either be ordinarily uncraftable / non-renewable or
make more sense than the standard recipes.

The current build should be compatible with Minecraft Java edition
1.20.2 – 1.21.4 (data pack formats 18–61).

## Recipes

* #### Bell
  - 3 sticks across the top
  - 5 gold ingots in a bell shape (⊓)
  - 1 gold nugget at the bottom

  There is no vanilla crafting recipe for a bell; normally they can
  only be found in villages; trading with armorers, toolsmiths, or
  weaponsmiths; or there’s a small chance of finding them in chests
  next to ruined portals.

* #### Bundle
  - 1 string
  - 1 leather

  Up through Minecraft 1.21.1, the bundle recipe required enabling an
  experimental data pack that comes with Minecraft.  This provides the
  same recipe.

  Since Minecraft 1.21.2, the bundle recipe is available in vanilla
  Minecraft.

* #### Buttons (stonecutter)

  There are stonecutter recipes to convert blackstone, polished
  blackstone, or stone into 8 buttons of that stone type, which is a
  higher yield than the crafting table recipes which only produce 1
  button per block; or cobblestone into 2 stone buttons, which
  normally can’t be crafted into buttons at all.  (For the wood button
  types, see the Sawmill data pack.)

* #### Fungus
  * ##### Crimson
    - 1 red mushroom
    - 1 crimson roots
  * ##### Warped
    - 1 brown mushroom
	- 1 warped roots

  Fungus may normally be grown by using bone meal on a patch of
  nylium; however it’s produced at a much lower rate than roots.
  These recipes allow you to obtain fungus more easily if you happen
  to have a lot of mushrooms.

* #### Leather
  - 4 rotten flesh in a square

  Makes 1 leather.  Normally leather is obtained from farming bovines
  or equines; this recipe is just a means for using excess rotten
  flesh you may have from zombies, as otherwise it is only useful for
  trading with clerics or as a (poor) emergency food source.

  - smelt (or smoke) cooked steak

  If you’re farming cows for leather, this is a way to turn excess
  steak into more leather.  You just have to cook the steak twice.

* #### Name Tag
  - 1 copper ingot
  - 1 chain

  There is no vanilla crafting recipe for a name tag; normally they
  can only be found randomly by fishing or through trading with
  librarians.

* #### Nether Fence (stonecutter)

  There’s a stonecutter recipe to turn a block of nether bricks into a
  nether fence.  This is a bit less efficient than the crafting recipe
  (yields 25% fewer fences) but lets you be more precise in the number
  of fences you get out.


* #### Nether quartz
  - 1 quartz block

  This recipe converts quartz blocks into nether quartz, using the
  same ratio by which quartz is normally converted to blocks.  This
  allows you to more efficiently store quartz and convert it back when
  needed.

* #### Redstone
  - 1 red sand
  - 1 block of raw copper
  - 1 block of quartz
  - 1 magma block

  Makes 4 blocks of redstone.  Normally redstone dust is mined from
  redstone ore or looted from witches; this recipe provides another
  means of obtaining it in larger quantities, particularly if you do a
  lot of digging in the nether.

* #### Sand (stonecutter)

  There are stonecutter recipes to convert netherrack into either sand
  or red sand.  This is useful if you happen to be digging a _lot_ of
  area out of the nether.  Although sand is plentiful in deserts, it
  is an otherwise non-renewable resource.

* #### Saplings
  * ##### Dark Oak
    - 1 stripped dark oak log
	- 1 dark oak leaves
  * ##### Jungle
    - 1 stripped jungle log
	- 1 vine

  Each recipe makes 2 saplings.  Dark oak and jungle saplings have a
  lower drop rate from breaking leaves than the other trees in
  Minecraft, so these recipes can help out with farming these trees,
  especially if you need to harvest their leaves for other purposes.


## Usage

### New worlds

From the Create New World screen, go to the “More” tab, click on “Data
Packs”, and “Open Pack Folder”.  Copy `MiscRecipes-`_version_`.zip`
into this folder, then it will appear in the list of “Available” data
packs.  Click the rightward arrow on the data pack icon to select it
for your world.

### Existing worlds

From the “Select World” screen, click on your world (don’t click its
play button) then click the “Edit” button.  Click the “Open World
Folder” and then open the `datapacks` folder within it.  Copy
`MiscRecipes-`_version_`.zip` into this folder.  Return to Minecraft’s
Edit World screen and click “Cancel”.  Then start your world; it will
automatically load the new data pack.
